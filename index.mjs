import { nanoid } from "nanoid";
import fs from "fs";

const couples = [
  "john-and-rosemary",
  "cameron-and-lydia",
  "james-and-camille",
  "matt-and-cristelle",
  "chad-and-amy",
];

const invalidNeighbours = {
  "john-and-rosemary": ["chad-and-amy", "cameron-and-lydia"],
  "cameron-and-lydia": ["john-and-rosemary", "james-and-camille"],
  "james-and-camille": ["cameron-and-lydia", "matt-and-cristell"],
  "matt-and-cristelle": ["james-and-camille", "chad-and-amy"],
  "chad-and-amy": ["matt-and-cristelle", "john-and-rosemary"],
};

function getOrder() {
  const randomised = couples
    .map((name) => ({
      name,
      sortOrder: Math.random(),
    }))
    .sort((a, b) => a.sortOrder - b.sortOrder)
    .map(({ name }) => name);

  return [
    [randomised[0], randomised[1]],
    [randomised[1], randomised[2]],
    [randomised[2], randomised[3]],
    [randomised[3], randomised[4]],
    [randomised[4], randomised[0]],
  ];
}

function getNeighbourScore(pairs) {
  return pairs.filter(([giver, receiver]) =>
    invalidNeighbours[giver].includes(receiver)
  ).length;
}

let pairs = getOrder();
let score = getNeighbourScore(pairs);
let attempts = 1;
while (score > 3) {
  pairs = getOrder();
  score = getNeighbourScore(pairs);
  attempts++;
}

const humanReadable = {
  "john-and-rosemary": "Mum and Dad",
  "cameron-and-lydia": "Cameron and Lydia",
  "james-and-camille": "James and Camille",
  "matt-and-cristelle": "Matt and Cristelle",
  "chad-and-amy": "Chad and Amy",
};

function getMessage(giver, receiver) {
  let paragraphs = [
    `Dear ${humanReadable[giver]},`,
    `For the secret santa this year you will be buying for ${humanReadable[receiver]}.`,
    `The budget is $50 in total, so you could buy one shared $50 present or two $25 presents (you get the idea). If you choose to buy one shared present, please make sure it's something they will both enjoy!`,
  ];

  switch (receiver) {
    case "cameron-and-lydia":
      paragraphs.push(
        `Lydia has reported that she likes being asked for present ideas, so feel free to get in contact with her.`
      );
      break;
    case "james-and-camille":
      paragraphs.push(
        `Camille has reported that she likes being asked for present ideas, so feel free to get in contact with her. James doesn't want to be asked.`
      );
      break;
    case "matt-and-cristelle":
      paragraphs.push(
        `Cristelle has reported that she likes being asked for present ideas, so feel free to get in contact with her. Matt doesn't want to be asked.`
      );
      break;
    case "chad-and-amy":
      paragraphs.push(
        `Chad has reported that he likes being asked for present ideas, so feel free to get in contact with him. Amy doesn't want to be asked.`
      );
      break;
  }

  return paragraphs.map((p) => `<p>	${p}</p>`).join("\n");
}

for (const [giver, receiver] of pairs) {
  const message = getMessage(giver, receiver);
  const filename = nanoid(10);

  fs.writeFileSync(
    `./public/${filename}.html`,
    `<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Secret Santa 2021</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
  <div id="content">
	${message}
    </div>
  </body>
</html>`
  );

  console.log(
    `${humanReadable[giver]} should be sent this link: https://chadoliver.gitlab.io/secret-santa/${filename}.html`
  );
}
